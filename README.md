<p align="center">
    <img src="https://www.mobly.com.br/images/mobly4/logo-mobly4.svg" width="250">
</p>

# Mobly Front base

## Build Setup

``` bash
# install

npm i git+ssh://git@bitbucket.org:mobly/mobly-front-base.git

add in the project '<style lang="scss">' tag the following call

@import "~moblystrap/main";

```

## How to use

Use of buttons will need class `btn` 

Exemple: `<button type="button" class="btn btn-green"></button>`

---

Button class colors
`btn-green`, `btn-purple`, `btn-orange`, `btn-blue-navy`, `btn-yellow`

Exemple: `<button type="button" class="btn btn-purple"></button>`

---

Button class outline 
`btn-outline-green`

Exemple: `<button type="button" class="btn btn-outline-purple"></button>`

---

Button Min Width
 
Min Width 162px 
`btn.mw-1 `

Min Width 180px 
`btn.mw-2`

Exemple: `<button type="button" class="btn btn-green mw1"></button>`

---

Button with arrows 

Left `arrow left`

Right `arrow right`

Exemple Left:
 ```
 <button type="button" class="btn btn-wrap-arrow">
   <div class="arrow left"></div>
 </button>
 ```            

Exemple Right:
 ```
<button type="button" class="btn btn-wrap-arrow">
    <div class="arrow right"></div>
</button>
 ```

---

Button pagination
`btn-wrap-pagination`, `btn-wrap-pagination:hover`

Exemple: `<button type="button" class="btn btn-wrap-pagination"> 2 </button>`

---

Button pagination outline 
`btn-outline-pagination`, `btn-outline-pagination:hover`

Exemple: `<button type="button" class="btn btn-outline-pagination"> 2 </button>`

---
